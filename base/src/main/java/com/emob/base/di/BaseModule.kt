package com.emob.base.di

import android.content.Context
import com.emob.base.ResourceProvider
import dagger.Module
import dagger.Provides


@Module
class BaseModule {

    //todo: Singleton?
    @Provides
    fun provideResourceProvider(context: Context): ResourceProvider {
        return ResourceProvider(context)
    }

}