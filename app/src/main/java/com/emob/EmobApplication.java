package com.emob;

import android.os.StrictMode;

import com.emob.di.DaggerEmobComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;

/**
 * The base application class that uses {@link DaggerApplication} to integrate the Dependency injection library Dagger 2
 */
public class EmobApplication extends DaggerApplication {

    private Thread.UncaughtExceptionHandler androidDefaultUEH;
    // Catch exception when app crashes and restart
    private Thread.UncaughtExceptionHandler handler = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            //Timber.e(ex);
            androidDefaultUEH.uncaughtException(thread, ex);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        androidDefaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(handler);

        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectAll()   // or .detectAll() for all detectable problems
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerEmobComponent.factory().create(this);
    }

}