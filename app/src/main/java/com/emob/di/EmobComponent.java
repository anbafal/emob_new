package com.emob.di;

import com.emob.EmobApplication;
import com.emob.base.di.BaseModule;
import com.emob.presentation.di.modules.PresentationModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {AndroidSupportInjectionModule.class, AppModule.class, PresentationModule.class, BaseModule.class})
public interface EmobComponent extends AndroidInjector<EmobApplication> {

    @Component.Factory
    interface Factory extends AndroidInjector.Factory<EmobApplication> {
    }
}