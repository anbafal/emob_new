package com.emob.di;

import android.content.Context;

import com.emob.EmobApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    /**
     * @param application this is the application class that is used
     * @return the application context whenever a context type is injected
     */
    @Singleton
    @Provides
    Context provideContext(EmobApplication application) {
        return application;
    }
}
