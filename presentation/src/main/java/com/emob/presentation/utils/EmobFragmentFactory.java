package com.emob.presentation.utils;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentFactory;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * This is the fragment factory that extends from {@link FragmentFactory} that is responsible for creating and injecting Fragments just like {@link com.emob.presentation.utils.ViewModelFactory}
 */
public class EmobFragmentFactory extends FragmentFactory {

    private final Map<Class<? extends Fragment>, Provider<Fragment>> creators;

    @Inject
    public EmobFragmentFactory(Map<Class<? extends Fragment>, Provider<Fragment>> creators) {
        this.creators = creators;
    }

    @NonNull
    @Override
    public Fragment instantiate(@NonNull ClassLoader classLoader, @NonNull String className) {
        Class<? extends Fragment> fragmentClass = loadFragmentClass(classLoader, className);
        Provider<? extends Fragment> creator = creators.get(fragmentClass);

        if (creator == null) {
            for (Map.Entry<Class<? extends Fragment>, Provider<Fragment>> entry : creators.entrySet()) {
                if (fragmentClass.isAssignableFrom(entry.getKey())) {
                    creator = entry.getValue();
                    break;
                }
            }
        }
        if (creator == null) {
            throw new IllegalArgumentException("unknown model class " + fragmentClass);
        }
        try {
            return creator.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}