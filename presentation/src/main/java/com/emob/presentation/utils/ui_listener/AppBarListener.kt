package com.emob.presentation.utils.ui_listener

interface AppBarListener {

    fun setUpAppBar(shouldDisplay: Boolean)

    fun setUpTitle(title: String)
}
