package com.emob.presentation.utils;

import androidx.fragment.app.Fragment;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import dagger.MapKey;

/**
 * This is an annotation to be used in order to add {@link Fragment} in the creators Map in the {@link EmobFragmentFactory} to create the corresponding {@link Fragment}
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@MapKey
public @interface FragmentKey {
    Class<? extends Fragment> value();
}