package com.emob.presentation.utils.ui_listener

interface BottomNavListener {
    fun setUpBottomNav(shouldDisplay: Boolean)
}