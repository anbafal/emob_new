@file:JvmName("ExtensionsUtils")

package com.emob.presentation.utils

import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.NavController

/* Useful for share view model in sub graph navigation to avoid memory leaks */
fun NavController.getCurrentGraphViewModelStore(): ViewModelStoreOwner {
    return getViewModelStoreOwner(graph.id)
}