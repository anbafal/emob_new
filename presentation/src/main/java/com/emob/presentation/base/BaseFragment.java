package com.emob.presentation.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;

import com.emob.presentation.di.EmobNavHostFragment;
import com.emob.presentation.features.SharedViewModel;
import com.emob.presentation.utils.ViewModelFactory;
import com.emob.presentation.utils.ui_listener.TBListener;

import dagger.android.support.DaggerFragment;


public abstract class BaseFragment<VDB extends ViewDataBinding, VM extends ViewModel> extends DaggerFragment {

    protected VDB binding;

    protected VM mViewModel;

    protected NavController navController;

    protected ViewModelFactory viewModelFactory;

    protected SharedViewModel sharedViewModel;

    protected boolean setAppBar = true;

    protected boolean setBottomNav = true;

    private TBListener tbListener;

    protected BaseFragment(ViewModelFactory viewModelFactory) {
        this.viewModelFactory = viewModelFactory;
    }

    protected BaseFragment() {
    }

    protected abstract Class<VM> getViewModelClass();

    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract void initFragment(VDB binding);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        binding.setLifecycleOwner(getViewLifecycleOwner());
        initFragment(binding);
        return binding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TBListener) {
            tbListener = (TBListener) context;
        } else {
            throw new RuntimeException("Parent Activity must implement TMBListener");
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        navController = EmobNavHostFragment.findNavController(this);
        mViewModel = new ViewModelProvider(this, viewModelFactory).get(getViewModelClass());
        if (getActivity() != null) {
            sharedViewModel = new ViewModelProvider(getActivity(), viewModelFactory).get(SharedViewModel.class);
        }

        tbListener.setUpAppBar(setAppBar);
        tbListener.setUpBottomNav(setBottomNav);
        sharedViewModel.getTitleLiveData().observe(getViewLifecycleOwner(), title -> {
            tbListener.setUpTitle(title);
        });
    }

}
