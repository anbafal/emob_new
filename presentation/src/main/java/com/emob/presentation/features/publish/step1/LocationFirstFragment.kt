package com.emob.presentation.features.publish.step1

import android.os.Bundle
import com.emob.presentation.R
import com.emob.presentation.databinding.LocationFirstFragmentBinding
import com.emob.presentation.features.publish.base.PublishFragment
import com.emob.presentation.features.publish.base.entities.Step
import com.emob.presentation.utils.ViewModelFactory
import javax.inject.Inject

class LocationFirstFragment @Inject constructor(viewModelFactory: ViewModelFactory) :
    PublishFragment<LocationFirstFragmentBinding, LocationFirstViewModel>(viewModelFactory) {


    override fun getViewModelClass() = LocationFirstViewModel::class.java

    override fun getLayoutRes() = R.layout.location_first_fragment

    override fun getStepBinding() = binding.currentStatusFirst

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        locationSharedViewModel.onLoadPart(Step.FIRST)

        binding.firstStepButton.setOnClickListener { navController.navigate(LocationFirstFragmentDirections.actionLocationFirstFragmentToLocationSecondFragment()) }


    }

}
