package com.emob.presentation.features.home.manage.my_annonce

import com.emob.presentation.R
import com.emob.presentation.base.BaseFragment
import com.emob.presentation.databinding.MyAnnonceFragmentBinding
import com.emob.presentation.utils.ViewModelFactory
import javax.inject.Inject

class MyAnnonceFragment @Inject constructor(viewModelFactory: ViewModelFactory) :
    BaseFragment<MyAnnonceFragmentBinding, MyAnnonceViewModel>(viewModelFactory) {


    override fun getViewModelClass() = MyAnnonceViewModel::class.java

    override fun getLayoutRes() = R.layout.my_annonce_fragment

    override fun initFragment(binding: MyAnnonceFragmentBinding?) {

    }

}
