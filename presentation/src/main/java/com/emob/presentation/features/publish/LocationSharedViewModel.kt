package com.emob.presentation.features.publish

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.emob.presentation.features.publish.base.entities.Step
import com.emob.presentation.features.publish.base.entities.StepUI
import javax.inject.Inject

class LocationSharedViewModel @Inject constructor() : ViewModel() {

    private val _stepLiveData = MutableLiveData<StepUI>()
    val stepLiveData: LiveData<StepUI>
        get() = _stepLiveData

    fun onLoadPart(step: Step) {
        val sizeEnum = Step.values().size
        return when (step) {
            Step.FIRST -> _stepLiveData.value = StepUI(1, sizeEnum)
            Step.SECOND -> _stepLiveData.value = StepUI(2, sizeEnum)
            else -> _stepLiveData.value = StepUI(3, sizeEnum)
        }
    }
}