package com.emob.presentation.features.publish.base.entities

enum class Action {
    CREATE_LOCATION, CREATE_SELL, SEARCH
}