package com.emob.presentation.features.splash

import com.emob.presentation.R
import com.emob.presentation.base.BaseFragment
import com.emob.presentation.databinding.SplashFragmentBinding
import com.emob.presentation.utils.ViewModelFactory
import javax.inject.Inject

class SplashFragment @Inject constructor(viewModelFactory: ViewModelFactory) :
    BaseFragment<SplashFragmentBinding, SplashViewModel>(viewModelFactory) {

    override fun getViewModelClass() = SplashViewModel::class.java

    override fun getLayoutRes() = R.layout.splash_fragment

    override fun initFragment(binding: SplashFragmentBinding?) {
        binding!!.fragment = this
        setAppBar = false
        setBottomNav = false
    }


    fun onTestModeClicked() {
        navController.navigate(SplashFragmentDirections.actionSplashDestToHomeFragment())
    }

}
