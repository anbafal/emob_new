package com.emob.presentation.features.settings.about

import android.os.Bundle
import com.emob.base.ResourceProvider
import com.emob.presentation.R
import com.emob.presentation.base.BaseFragment
import com.emob.presentation.databinding.AboutFragmentBinding
import com.emob.presentation.utils.ViewModelFactory
import javax.inject.Inject

class AboutFragment @Inject constructor(viewModelFactory: ViewModelFactory, private val resourceProvider: ResourceProvider) :
    BaseFragment<AboutFragmentBinding, AboutViewModel>(viewModelFactory) {

    override fun getViewModelClass() = AboutViewModel::class.java

    override fun getLayoutRes() = R.layout.about_fragment

    override fun initFragment(binding: AboutFragmentBinding?) {
        setBottomNav = false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedViewModel.setTitle(resourceProvider.getString(R.string.settings_title_about))
    }

}
