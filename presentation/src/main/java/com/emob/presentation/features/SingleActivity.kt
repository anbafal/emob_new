package com.emob.presentation.features

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.emob.base.ResourceProvider
import com.emob.presentation.R
import com.emob.presentation.base.BaseActivity
import com.emob.presentation.databinding.ActivitySingleBinding
import com.emob.presentation.di.EmobNavHostFragment
import com.emob.presentation.utils.ViewModelFactory
import com.emob.presentation.utils.ui_listener.TBListener
import javax.inject.Inject


class SingleActivity : BaseActivity(), TBListener {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var resourceProvider: ResourceProvider

    private lateinit var appBarConfiguration: AppBarConfiguration

    private lateinit var navController: NavController

    private lateinit var binding: ActivitySingleBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_single)
        setSupportActionBar(binding.toolbar)

        // exclude fragment that we don't want that arrow for back is displayed
        appBarConfiguration = AppBarConfiguration(setOf(R.id.home_fragment, R.id.manage_fragment))

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as EmobNavHostFragment

        navController = navHostFragment.navController
        navController.apply {
            NavigationUI.setupActionBarWithNavController(this@SingleActivity, this, appBarConfiguration)
            NavigationUI.setupWithNavController(binding.bottomNav, this)
        }


    }


    override fun setUpBottomNav(shouldDisplay: Boolean) {
        binding.bottomNav.visibility = if (shouldDisplay) View.VISIBLE else View.GONE
    }

    override fun setUpAppBar(shouldDisplay: Boolean) {
        binding.appbar.visibility = if (shouldDisplay) View.VISIBLE else View.GONE
    }

    override fun setUpTitle(title: String) {
        navController.currentDestination?.label = title
        binding.toolbar.title = title
    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navController, appBarConfiguration) || super.onSupportNavigateUp()
    }


}