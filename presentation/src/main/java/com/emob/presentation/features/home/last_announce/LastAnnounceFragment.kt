package com.emob.presentation.features.home.last_announce

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.navigation.ui.onNavDestinationSelected
import com.emob.base.ResourceProvider
import com.emob.presentation.R
import com.emob.presentation.base.BaseFragment
import com.emob.presentation.databinding.LastAnnounceFragmentBinding
import com.emob.presentation.features.home.last_announce.entities.FLOATING_ARGUMENT_NAME
import com.emob.presentation.features.home.last_announce.entities.FloatingUI
import com.emob.presentation.utils.ViewModelFactory
import javax.inject.Inject

class LastAnnounceFragment @Inject constructor(viewModelFactory: ViewModelFactory, private val resourceProvider: ResourceProvider) :
    BaseFragment<LastAnnounceFragmentBinding, LastAnnounceViewModel>(viewModelFactory) {


    override fun getViewModelClass() = LastAnnounceViewModel::class.java

    override fun getLayoutRes() = R.layout.last_announce_fragment

    override fun initFragment(binding: LastAnnounceFragmentBinding?) {

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        sharedViewModel.setTitle(resourceProvider.getString(R.string.home_title))
        initFloatingBottomMenu()

        mViewModel.floatingActionLiveData.observe(viewLifecycleOwner, Observer { floatingMenuObserver(it) })
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main_settings, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(navController) || super.onOptionsItemSelected(item)
    }

    private fun initFloatingBottomMenu() {
        binding.floatingBottomMenu.apply {
            inflate(R.menu.floating_menu_home)
            setOnActionSelectedListener { actionItem ->
                mViewModel.onClickFloatingMenu(actionItem.id)
                true
            }
        }
    }

    private fun floatingMenuObserver(floatingUI: FloatingUI) {
        val bundle = Bundle()
        bundle.putSerializable(FLOATING_ARGUMENT_NAME, floatingUI.action)
        navController.navigate(floatingUI.destinationId, bundle)
    }
}
