package com.emob.presentation.features.publish.base

import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.emob.presentation.base.BaseFragment
import com.emob.presentation.databinding.CommonPublishStepBinding
import com.emob.presentation.features.publish.LocationSharedViewModel
import com.emob.presentation.features.publish.base.entities.StepUI
import com.emob.presentation.utils.ViewModelFactory
import com.emob.presentation.utils.getCurrentGraphViewModelStore

abstract class PublishFragment<VDB : ViewDataBinding, VM : ViewModel> constructor(viewModelFactory: ViewModelFactory) :
    BaseFragment<VDB, VM>(viewModelFactory) {

    protected lateinit var locationSharedViewModel: LocationSharedViewModel

    abstract fun getStepBinding(): CommonPublishStepBinding


    @CallSuper
    override fun initFragment(binding: VDB) {
        setAppBar = false
        setBottomNav = false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        locationSharedViewModel =
            ViewModelProvider(navController.getCurrentGraphViewModelStore(), viewModelFactory).get(LocationSharedViewModel::class.java)

        locationSharedViewModel.stepLiveData.observe(viewLifecycleOwner, Observer {
            currentStep(it)
        })
    }

    private fun currentStep(stepUI: StepUI) {
        getStepBinding().statusViewScroller.statusView.run {
            currentCount = stepUI.currentStep
            stepCount = stepUI.stepCount
        }
    }
}