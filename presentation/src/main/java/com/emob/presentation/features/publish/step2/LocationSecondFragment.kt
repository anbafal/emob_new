package com.emob.presentation.features.publish.step2

import android.os.Bundle
import com.emob.presentation.R
import com.emob.presentation.databinding.LocationSecondFragmentBinding
import com.emob.presentation.features.publish.base.PublishFragment
import com.emob.presentation.features.publish.base.entities.Step
import com.emob.presentation.utils.ViewModelFactory
import javax.inject.Inject

class LocationSecondFragment @Inject constructor(viewModelFactory: ViewModelFactory) :
    PublishFragment<LocationSecondFragmentBinding, LocationSecondViewModel>(viewModelFactory) {

    override fun getViewModelClass() = LocationSecondViewModel::class.java

    override fun getLayoutRes() = R.layout.location_second_fragment

    override fun getStepBinding() = binding.head.currentStatusSecond


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        locationSharedViewModel.onLoadPart(Step.SECOND)

        binding.other.secondStepButton.setOnClickListener { navController.navigate(LocationSecondFragmentDirections.actionLocationSecondFragmentToLocationThirdFragment()) }
    }


}
