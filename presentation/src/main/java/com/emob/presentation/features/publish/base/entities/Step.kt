package com.emob.presentation.features.publish.base.entities

enum class Step {
    FIRST, SECOND, THIRD
}