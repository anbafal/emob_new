package com.emob.presentation.features

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.emob.base.ResourceProvider
import javax.inject.Inject

/**
 * This the viewModel for the only activity in the application. And it is shared among all fragments and used to send data back and forth
 */
class SharedViewModel @Inject constructor(resourceProvider: ResourceProvider) : ViewModel() {

    private val _titleLiveData = MutableLiveData<String>()
    val titleLiveData: LiveData<String>
        get() = _titleLiveData


    fun setTitle(title: String) {
        _titleLiveData.value = title
    }


}