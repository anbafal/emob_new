package com.emob.presentation.features.settings.give_feedback

import android.os.Bundle
import com.emob.base.ResourceProvider
import com.emob.presentation.R
import com.emob.presentation.base.BaseFragment
import com.emob.presentation.databinding.FeedbackFragmentBinding
import com.emob.presentation.utils.ViewModelFactory
import javax.inject.Inject

class FeedbackFragment @Inject constructor(viewModelFactory: ViewModelFactory, private val resourceProvider: ResourceProvider) :
    BaseFragment<FeedbackFragmentBinding, FeedbackViewModel>(viewModelFactory) {

    override fun getViewModelClass() = FeedbackViewModel::class.java

    override fun getLayoutRes() = R.layout.feedback_fragment

    override fun initFragment(binding: FeedbackFragmentBinding?) {
        setBottomNav = false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedViewModel.setTitle(resourceProvider.getString(R.string.settings_title_enhance))
    }

}
