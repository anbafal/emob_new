package com.emob.presentation.features.publish.step3

import android.os.Bundle
import com.emob.presentation.R
import com.emob.presentation.databinding.LocationThirdFragmentBinding
import com.emob.presentation.features.publish.base.PublishFragment
import com.emob.presentation.features.publish.base.entities.Step
import com.emob.presentation.utils.ViewModelFactory
import javax.inject.Inject

class LocationThirdFragment @Inject constructor(viewModelFactory: ViewModelFactory) :
    PublishFragment<LocationThirdFragmentBinding, LocationThirdViewModel>(viewModelFactory) {

    override fun getViewModelClass() = LocationThirdViewModel::class.java

    override fun getLayoutRes() = R.layout.location_third_fragment

    override fun getStepBinding() = binding.currentStatusThird

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        locationSharedViewModel.onLoadPart(Step.THIRD)
    }

}
