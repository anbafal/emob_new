package com.emob.presentation.features.home.last_announce

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.emob.base.ResourceProvider
import com.emob.presentation.R
import com.emob.presentation.features.home.last_announce.entities.FloatingUI
import com.emob.presentation.features.publish.base.entities.Action
import com.emob.presentation.utils.SingleLiveEvent
import javax.inject.Inject

class LastAnnounceViewModel @Inject constructor(private val resourceProvider: ResourceProvider) : ViewModel() {

    private val _floatingActionLiveData = SingleLiveEvent<FloatingUI>()
    val floatingActionLiveData: LiveData<FloatingUI>
        get() = _floatingActionLiveData


    fun onClickFloatingMenu(menuId: Int) {

        val floatingUI = when (menuId) {
            R.id.floating_add_location -> FloatingUI(R.id.action_home_fragment_to_publish_location_navigation, Action.CREATE_LOCATION)
            R.id.floating_add_selling -> FloatingUI(R.id.action_home_fragment_to_publish_location_navigation, Action.CREATE_SELL)
            else -> FloatingUI(R.id.action_home_fragment_to_publish_location_navigation, Action.SEARCH)
        }
        _floatingActionLiveData.value = floatingUI
    }


}
