package com.emob.presentation.features.home.manage.bookmark

import com.emob.presentation.R
import com.emob.presentation.base.BaseFragment
import com.emob.presentation.databinding.BookmarkFragmentBinding
import com.emob.presentation.utils.ViewModelFactory
import javax.inject.Inject

class BookmarkFragment @Inject constructor(viewModelFactory: ViewModelFactory) :
    BaseFragment<BookmarkFragmentBinding, BookmarkViewModel>(viewModelFactory) {

    override fun getViewModelClass() = BookmarkViewModel::class.java
    override fun getLayoutRes() = R.layout.bookmark_fragment

    override fun initFragment(binding: BookmarkFragmentBinding?) {

    }


}
