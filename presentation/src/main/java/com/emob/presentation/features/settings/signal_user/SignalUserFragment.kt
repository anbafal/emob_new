package com.emob.presentation.features.settings.signal_user

import android.os.Bundle
import com.emob.base.ResourceProvider
import com.emob.presentation.R
import com.emob.presentation.base.BaseFragment
import com.emob.presentation.databinding.SignalUserFragmentBinding
import com.emob.presentation.utils.ViewModelFactory
import javax.inject.Inject

class SignalUserFragment @Inject constructor(viewModelFactory: ViewModelFactory, private val resourceProvider: ResourceProvider) :
    BaseFragment<SignalUserFragmentBinding, SignalUserViewModel>(viewModelFactory) {

    override fun getViewModelClass() = SignalUserViewModel::class.java

    override fun getLayoutRes() = R.layout.signal_user_fragment

    override fun initFragment(binding: SignalUserFragmentBinding?) {
        setBottomNav = false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        sharedViewModel.setTitle(resourceProvider.getString(R.string.settings_title_signal))
    }
}
