package com.emob.presentation.features.publish.base.entities

data class StepUI(val currentStep: Int, val stepCount: Int)