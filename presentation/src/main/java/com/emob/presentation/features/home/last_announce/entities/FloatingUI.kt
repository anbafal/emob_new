package com.emob.presentation.features.home.last_announce.entities

import com.emob.presentation.features.publish.base.entities.Action

const val FLOATING_ARGUMENT_NAME = "action"

data class FloatingUI(val destinationId: Int, val action: Action)