package com.emob.presentation.features.home.manage

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.emob.base.ResourceProvider
import com.emob.presentation.R
import com.emob.presentation.base.BaseFragment
import com.emob.presentation.databinding.ManageFragmentBinding
import com.emob.presentation.features.home.manage.bookmark.BookmarkFragment
import com.emob.presentation.features.home.manage.my_annonce.MyAnnonceFragment
import com.emob.presentation.utils.ViewModelFactory
import com.google.android.material.tabs.TabLayoutMediator
import javax.inject.Inject


class ManageFragment @Inject constructor(viewModelFactory: ViewModelFactory, private val resourceProvider: ResourceProvider) :
    BaseFragment<ManageFragmentBinding, ManageViewModel>(viewModelFactory) {


    override fun getViewModelClass() = ManageViewModel::class.java

    override fun getLayoutRes() = R.layout.manage_fragment

    override fun initFragment(binding: ManageFragmentBinding?) {

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        sharedViewModel.setTitle(resourceProvider.getString(R.string.manage_title))

        val pagerAdapter = ScreenSlidePagerAdapter(this)
        binding?.manageViewPager?.adapter = pagerAdapter

        TabLayoutMediator(binding!!.manageTabLayout, binding.manageViewPager) { tab, position ->
            tab.text = mViewModel.getTabTitle(position)
        }.attach()
    }


    private inner class ScreenSlidePagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

        override fun getItemCount(): Int = Companion.NUM_PAGES

        override fun createFragment(position: Int): Fragment {

            return when (position) {
                0 -> MyAnnonceFragment(viewModelFactory)
                1 -> BookmarkFragment(viewModelFactory)
                else -> MyAnnonceFragment(viewModelFactory)
            }

        }

    }

    companion object {
        private const val NUM_PAGES = 2
    }
}


