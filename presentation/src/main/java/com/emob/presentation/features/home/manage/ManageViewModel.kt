package com.emob.presentation.features.home.manage

import androidx.lifecycle.ViewModel
import com.emob.base.ResourceProvider
import com.emob.presentation.R
import javax.inject.Inject

class ManageViewModel @Inject constructor(private val resourceProvider: ResourceProvider) : ViewModel() {

    fun getTabTitle(position: Int): String {

        return when (position) {
            0 -> resourceProvider.getString(R.string.manage_title_my_announce)
            1 -> resourceProvider.getString(R.string.manage_title_savings)
            else -> resourceProvider.getString(R.string.manage_title_my_announce)
        }

    }
}
