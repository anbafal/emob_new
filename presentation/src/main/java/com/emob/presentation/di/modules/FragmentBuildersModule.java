package com.emob.presentation.di.modules;

import com.emob.presentation.features.home.last_announce.LastAnnounceFragment;
import com.emob.presentation.features.home.manage.ManageFragment;
import com.emob.presentation.features.home.manage.bookmark.BookmarkFragment;
import com.emob.presentation.features.home.manage.my_annonce.MyAnnonceFragment;
import com.emob.presentation.features.publish.step1.LocationFirstFragment;
import com.emob.presentation.features.publish.step2.LocationSecondFragment;
import com.emob.presentation.features.publish.step3.LocationThirdFragment;
import com.emob.presentation.features.settings.about.AboutFragment;
import com.emob.presentation.features.settings.give_feedback.FeedbackFragment;
import com.emob.presentation.features.settings.signal_user.SignalUserFragment;
import com.emob.presentation.features.splash.SplashFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuildersModule {

    @ContributesAndroidInjector()
    abstract SplashFragment contributesSplashFragment();

    @ContributesAndroidInjector()
    abstract LastAnnounceFragment contributesLastAnnounceFragment();

    @ContributesAndroidInjector()
    abstract MyAnnonceFragment contributesMyAnnonceFragment();

    @ContributesAndroidInjector()
    abstract ManageFragment contributesManageFragment();

    @ContributesAndroidInjector()
    abstract BookmarkFragment contributesBookmarkFragment();

    @ContributesAndroidInjector()
    abstract AboutFragment contributesAboutFragment();

    @ContributesAndroidInjector()
    abstract FeedbackFragment contributesFeedbackFragment();

    @ContributesAndroidInjector()
    abstract SignalUserFragment contributesSignalUserFragment();

    @ContributesAndroidInjector()
    abstract LocationFirstFragment contributesLocationFirstFragment();

    @ContributesAndroidInjector()
    abstract LocationSecondFragment contributesLocationSecondFragment();

    @ContributesAndroidInjector()
    abstract LocationThirdFragment contributesLocationThirdFragment();

}