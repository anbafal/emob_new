package com.emob.presentation.di.modules;

import androidx.lifecycle.ViewModel;

import com.emob.presentation.features.SharedViewModel;
import com.emob.presentation.features.home.last_announce.LastAnnounceViewModel;
import com.emob.presentation.features.home.manage.ManageViewModel;
import com.emob.presentation.features.home.manage.bookmark.BookmarkViewModel;
import com.emob.presentation.features.home.manage.my_annonce.MyAnnonceViewModel;
import com.emob.presentation.features.publish.LocationSharedViewModel;
import com.emob.presentation.features.publish.step1.LocationFirstViewModel;
import com.emob.presentation.features.publish.step2.LocationSecondViewModel;
import com.emob.presentation.features.publish.step3.LocationThirdViewModel;
import com.emob.presentation.features.settings.about.AboutViewModel;
import com.emob.presentation.features.settings.give_feedback.FeedbackViewModel;
import com.emob.presentation.features.settings.signal_user.SignalUserViewModel;
import com.emob.presentation.features.splash.SplashViewModel;
import com.emob.presentation.utils.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelsModule {

    /**
     * This method basically says
     * inject this object into a Map using the @IntoMap annotation,
     * with the  AuthFragmentViewModel.class as key,
     * and a Provider that will build a AuthFragmentViewModel
     * object.
     */

    @Binds
    @IntoMap
    @ViewModelKey(SharedViewModel.class)
    protected abstract ViewModel emobViewModel(SharedViewModel sharedViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel.class)
    protected abstract ViewModel splashViewModel(SplashViewModel splashViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AboutViewModel.class)
    protected abstract ViewModel aboutViewModel(AboutViewModel aboutViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(FeedbackViewModel.class)
    protected abstract ViewModel feedbackViewModel(FeedbackViewModel feedbackViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SignalUserViewModel.class)
    protected abstract ViewModel SignalUserViewModel(SignalUserViewModel signalUserViewModel);


    @Binds
    @IntoMap
    @ViewModelKey(LastAnnounceViewModel.class)
    protected abstract ViewModel lastAnnounceViewModel(LastAnnounceViewModel lastAnnounceViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ManageViewModel.class)
    protected abstract ViewModel manageViewModel(ManageViewModel manageViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MyAnnonceViewModel.class)
    protected abstract ViewModel myAnnounceViewModel(MyAnnonceViewModel myAnnonceViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(BookmarkViewModel.class)
    protected abstract ViewModel bookmarkViewModel(BookmarkViewModel bookmarkViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LocationFirstViewModel.class)
    protected abstract ViewModel locationFirstViewModel(LocationFirstViewModel locationFirstViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LocationSecondViewModel.class)
    protected abstract ViewModel locationSecondViewModel(LocationSecondViewModel locationSecondViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LocationThirdViewModel.class)
    protected abstract ViewModel locationThirdViewModel(LocationThirdViewModel locationThirdViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LocationSharedViewModel.class)
    protected abstract ViewModel locationSharedViewModel(LocationSharedViewModel locationSharedViewModel);

}
