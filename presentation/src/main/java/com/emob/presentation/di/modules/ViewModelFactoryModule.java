package com.emob.presentation.di.modules;

import androidx.lifecycle.ViewModelProvider;

import com.emob.presentation.utils.ViewModelFactory;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelFactoryModule {
    /**
     * @param factory the {@link ViewModelFactory} to be injected in viewModels and used to create different ViewModels intannces
     * @return the factory mentioned above
     */

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

}
