package com.emob.presentation.di.modules;

import com.emob.presentation.di.scopes.ActivityScope;
import com.emob.presentation.features.SingleActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class PresentationModule {

    /**
     * @return the {@link com.emob.presentation.features.SingleActivity} with all the injection that are needed and shares the modules between all fragments
     */
    @ActivityScope
    @ContributesAndroidInjector(modules = {NavHostModule.class, FragmentBuildersModule.class,
            FragmentBindingModule.class, ViewModelFactoryModule.class, ViewModelsModule.class
    })
    abstract SingleActivity contributeSingleActivity();
}
