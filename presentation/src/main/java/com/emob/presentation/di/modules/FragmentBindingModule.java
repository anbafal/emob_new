package com.emob.presentation.di.modules;

import androidx.fragment.app.Fragment;

import com.emob.presentation.features.home.last_announce.LastAnnounceFragment;
import com.emob.presentation.features.home.manage.ManageFragment;
import com.emob.presentation.features.home.manage.bookmark.BookmarkFragment;
import com.emob.presentation.features.home.manage.my_annonce.MyAnnonceFragment;
import com.emob.presentation.features.publish.step1.LocationFirstFragment;
import com.emob.presentation.features.publish.step2.LocationSecondFragment;
import com.emob.presentation.features.publish.step3.LocationThirdFragment;
import com.emob.presentation.features.settings.about.AboutFragment;
import com.emob.presentation.features.settings.give_feedback.FeedbackFragment;
import com.emob.presentation.features.settings.signal_user.SignalUserFragment;
import com.emob.presentation.features.splash.SplashFragment;
import com.emob.presentation.utils.FragmentKey;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
abstract class FragmentBindingModule {

    @Binds
    @IntoMap
    @FragmentKey(SplashFragment.class)
    abstract Fragment bindSplashFragment(SplashFragment fragment);

    @Binds
    @IntoMap
    @FragmentKey(AboutFragment.class)
    abstract Fragment bindAboutFragment(AboutFragment fragment);

    @Binds
    @IntoMap
    @FragmentKey(FeedbackFragment.class)
    abstract Fragment bindFeedbackFragment(FeedbackFragment fragment);

    @Binds
    @IntoMap
    @FragmentKey(SignalUserFragment.class)
    abstract Fragment bindSignalUserFragment(SignalUserFragment fragment);

    @Binds
    @IntoMap
    @FragmentKey(LastAnnounceFragment.class)
    abstract Fragment bindLastAnnounceFragment(LastAnnounceFragment fragment);


    @Binds
    @IntoMap
    @FragmentKey(ManageFragment.class)
    abstract Fragment bindManageFragment(ManageFragment fragment);

    @Binds
    @IntoMap
    @FragmentKey(MyAnnonceFragment.class)
    abstract Fragment bindMyAnnonceFragment(MyAnnonceFragment fragment);

    @Binds
    @IntoMap
    @FragmentKey(BookmarkFragment.class)
    abstract Fragment bindBookmarkFragment(BookmarkFragment fragment);

    @Binds
    @IntoMap
    @FragmentKey(LocationFirstFragment.class)
    abstract Fragment bindLocationFirstFragment(LocationFirstFragment fragment);

    @Binds
    @IntoMap
    @FragmentKey(LocationSecondFragment.class)
    abstract Fragment bindLocationSecondFragment(LocationSecondFragment fragment);

    @Binds
    @IntoMap
    @FragmentKey(LocationThirdFragment.class)
    abstract Fragment bindLocationThirdFragment(LocationThirdFragment fragment);


}
