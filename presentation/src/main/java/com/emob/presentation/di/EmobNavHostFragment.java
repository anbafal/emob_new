package com.emob.presentation.di;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.fragment.NavHostFragment;

import com.emob.presentation.utils.EmobFragmentFactory;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

/**
 * This is the host Fragment that is used instead of the default {@link NavHostFragment} in order to enable injection and {@link androidx.fragment.app.FragmentFactory} constructor creation
 */
public class EmobNavHostFragment extends NavHostFragment {

    @Inject
    protected EmobFragmentFactory fragmentFactory;


    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        getChildFragmentManager().setFragmentFactory(fragmentFactory);
        super.onCreate(savedInstanceState);
    }
}
