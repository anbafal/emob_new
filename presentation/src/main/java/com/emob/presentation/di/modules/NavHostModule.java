package com.emob.presentation.di.modules;

import com.emob.presentation.di.EmobNavHostFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * This is the module that provide the custom {@link androidx.navigation.fragment.NavHostFragment} for the application so we can use the {@link androidx.fragment.app.FragmentFactory}
 */
@Module
public abstract class NavHostModule {

    @ContributesAndroidInjector(modules = {FragmentBindingModule.class})
    abstract EmobNavHostFragment navHostFragmentInjector();
}