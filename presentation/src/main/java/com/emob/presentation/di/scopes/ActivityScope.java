package com.emob.presentation.di.scopes;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * This is the scope that is used to limit the lifecycle of some objects to the lifecycle of an activity
 */
@Scope
@Documented
@Retention(RUNTIME)
public @interface ActivityScope {
}