package com.emob.domain.base.usecases

import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * This abstract class is shared among several closely related UseCase classes
 * that classes that extend this abstract class to use common methods & fields
 **/
abstract class CompletableUseCase<U> : UseCase() {

    protected abstract fun buildUseCaseCompletable(u: U? = null): Completable

    @JvmOverloads
    fun execute(u: U? = null, onComplete: (() -> Unit), onError: ((t: Throwable) -> Unit), onFinished: () -> Unit = {}) {

        val errorBlock: ((t: Throwable) -> Unit) = { error ->
            //Timber.e(error)
            onError(error)
        };
        disposeLast()
        lastDisposable =
            buildUseCaseCompletable(u).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).doAfterTerminate(onFinished)
                .subscribe(onComplete, errorBlock)
        lastDisposable?.let {
            compositeDisposable.add(it)
        }
    }

    @JvmOverloads
    fun execute(u: U? = null) {
        this.execute(u, {}, {}, {})
    }
}