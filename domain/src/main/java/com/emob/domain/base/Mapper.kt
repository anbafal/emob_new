package com.emob.domain.base

interface Mapper<in E, out T> {
    fun mapFrom(from: E): T
}