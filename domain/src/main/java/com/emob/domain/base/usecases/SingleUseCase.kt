package com.emob.domain.base.usecases


import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * This abstract class is shared among several closely related UseCase classes
 * that classes that extend this abstract class to use common methods & fields
 **/
abstract class SingleUseCase<T, U> : UseCase() {

    protected abstract fun buildUseCaseSingle(u: U? = null): Single<T>

    fun execute(u: U? = null, onSuccess: ((t: T) -> Unit), onError: ((t: Throwable) -> Unit) = {}, onFinished: () -> Unit = {}) {
        disposeLast()
        lastDisposable = buildUseCaseSingle(u).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).doAfterTerminate(onFinished)
            .subscribe(onSuccess, onError)

        lastDisposable?.let {
            compositeDisposable.add(it)
        }
    }

    //We do it because JvmOverloads doesn't add a mapper to omit the first parameter
    @JvmOverloads
    fun execute(onSuccess: ((t: T) -> Unit), onError: ((t: Throwable) -> Unit) = {}, onFinished: () -> Unit = {}) {
        execute(null, onSuccess, onError, onFinished)
    }

    @JvmOverloads
    fun executeImmediate(u: U? = null): T {
        disposeLast()
        return buildUseCaseSingle(u).blockingGet()
    }
}