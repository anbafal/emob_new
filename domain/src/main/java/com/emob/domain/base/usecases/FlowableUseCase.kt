package com.emob.domain.base.usecases


import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * This abstract class is shared among several closely related UseCase classes
 * that classes that extend this abstract class to use common methods & fields
 **/
abstract class FlowableUseCase<T, U> : UseCase() {

    protected abstract fun buildUseCaseFlowable(u: U): Flowable<T>

    fun execute(u: U, onSuccess: ((t: T) -> Unit), onError: ((t: Throwable) -> Unit), onFinished: () -> Unit = {}) {

        val errorBlock: ((t: Throwable) -> Unit) = { error ->
            //Timber.e(error)
            onError(error)
        }

        disposeLast()
        lastDisposable = buildUseCaseFlowable(u).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).doAfterTerminate(onFinished)
            .subscribe(onSuccess, errorBlock)

        lastDisposable?.let {
            compositeDisposable.add(it)
        }
    }

    fun executeImmediate(u: U): T? {
        disposeLast()
        return buildUseCaseFlowable(u).blockingFirst()
    }
}