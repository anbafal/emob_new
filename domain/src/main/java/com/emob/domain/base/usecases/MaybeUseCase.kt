@file:JvmName("MaybeUseCase")

package com.emob.domain.base.usecases


import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * This abstract class is shared among several closely related UseCase classes
 * that classes that extend this abstract class to use common methods & fields
 **/
abstract class MaybeUseCase<T, U> : UseCase() {

    protected abstract fun buildUseCaseMaybe(u: U? = null): Maybe<T>

    @JvmOverloads
    fun execute(u: U? = null, onSuccess: ((t: T) -> Unit), onError: ((t: Throwable) -> Unit), onFinished: () -> Unit = {}) {
        disposeLast()
        lastDisposable = buildUseCaseMaybe(u).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).doAfterTerminate(onFinished)
            .subscribe(onSuccess, onError)

        lastDisposable?.let {
            compositeDisposable.add(it)
        }
    }

    @JvmOverloads
    fun executeImmediate(u: U? = null): T? {
        disposeLast()
        return buildUseCaseMaybe(u).blockingGet()
    }
}